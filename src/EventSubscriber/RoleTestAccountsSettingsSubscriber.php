<?php

namespace Drupal\role_test_accounts\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\role_test_accounts\RoleTestAccountsManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Update role test accounts on configuration updates.
 */
class RoleTestAccountsSettingsSubscriber implements EventSubscriberInterface {

  public function __construct(
    protected RoleTestAccountsManagerInterface $roleTestAccountsManager,
  ) {
  }

  /**
   * Update role test accounts on configuration updates.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Event to process.
   */
  public function onSave(ConfigCrudEvent $event) {
    if ($event->getConfig()->getName() === 'role_test_accounts.settings') {
      $config = $event->getConfig();

      $this->roleTestAccountsManager->generateRoleTestAccounts($config);

      if ($config->get('password') !== $config->getOriginal('password')) {
        $this->roleTestAccountsManager->setRoleTestAccountsPassword($config->get('password'));
      }
      if ($config->get('block_after') !== $config->getOriginal('block_after')) {
        $this->roleTestAccountsManager->setStatusForRoleTestAccounts();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onSave'];
    return $events;
  }

}
