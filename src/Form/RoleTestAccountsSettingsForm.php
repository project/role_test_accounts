<?php

namespace Drupal\role_test_accounts\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\RedundantEditableConfigNamesTrait;
use Drupal\Core\Form\ToConfig;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Provides a settings form for Role Test Accounts.
 */
class RoleTestAccountsSettingsForm extends ConfigFormBase {

  use RedundantEditableConfigNamesTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_test_accounts_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure role test account generation'),
      '#open' => TRUE,
    ];
    $form['roles']['selection_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Role selection method'),
      '#options' => [
        'exclude' => $this->t('Do not generate role test accounts for the roles below'),
        'include' => $this->t('Only generate role test accounts for the roles below'),
      ],
      '#config_target' => 'role_test_accounts.settings:selection_method',
    ];

    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    $roles = array_map(static fn(RoleInterface $role) => Html::escape($role->label()), $roles);
    $form['roles']['selected_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $roles,
      '#config_target' => new ConfigTarget(
        'role_test_accounts.settings',
        'selected_roles',
        toConfig: fn($value) => array_values(array_filter($value)),
      ),
    ];

    $form['authentication'] = [
      '#type' => 'details',
      '#title' => $this->t('Authentication settings'),
      '#open' => TRUE,
    ];
    $form['authentication']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Set a new password for all role test accounts'),
      '#description' => $this->t('Leave empty to keep the current password.'),
      '#config_target' => new ConfigTarget('role_test_accounts.settings', 'password', toConfig: fn(?string $value) => $value ?: ToConfig::NoOp),
    ];

    $form['block'] = [
      '#type' => 'details',
      '#title' => $this->t('Block inactive role test accounts'),
      '#open' => TRUE,
    ];
    $form['block']['block_after'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Block role test accounts after a period of inactivity.'),
      '#field_suffix' => $this->t('seconds'),
      '#description' => $this->t('Use 0 to disable this option.'),
      '#required' => TRUE,
      '#config_target' => 'role_test_accounts.settings:block_after',
    ];

    return $form;
  }

}
