<?php

namespace Drupal\role_test_accounts;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Implements the Role Test Accounts Manager interface.
 */
class RoleTestAccountsManager implements RoleTestAccountsManagerInterface {

  /**
   * The configuration for Role Test Accounts.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The site configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $siteConfig;

  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected TimeInterface $time,
  ) {
    $this->config = $this->configFactory->get('role_test_accounts.settings');
    $this->siteConfig = $this->configFactory->get('system.site');
  }

  /**
   * {@inheritdoc}
   */
  public function createTestAccount($role_id) {
    $user = user_load_by_name('test.' . $role_id);

    if (!$user) {
      $user_storage = $this->entityTypeManager->getStorage('user');
      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->create(['name' => 'test.' . $role_id]);
    }

    $site_mail = $this->siteConfig->get('mail');
    $site_mail_parts = explode('@', $site_mail);
    $site_mail_username = array_shift($site_mail_parts);
    $user->setEmail($site_mail_username . '+' . $role_id . '@' . implode('', $site_mail_parts));
    $user->setPassword($this->config->get('password'));
    $user->activate();
    if ($role_id !== AccountInterface::AUTHENTICATED_ROLE) {
      $user->addRole($role_id);
    }
    $user->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTestAccount($role_id) {
    $user = user_load_by_name('test.' . $role_id);
    if ($user instanceof UserInterface) {
      $user->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateRoleTestAccounts(Config $config = NULL) {
    if (!$config) {
      $config = $this->config;
    }
    $role_ids = $this->entityTypeManager->getStorage('user_role')->getQuery()->accessCheck(FALSE)->execute();
    unset($role_ids[AccountInterface::ANONYMOUS_ROLE]);

    if (!empty($config->getOriginal('selection_method') && $config->getOriginal('selection_method') === 'exclude')) {
      $original_roles = array_diff($role_ids, (array) $config->getOriginal('selected_roles'));
    }
    else {
      $original_roles = array_values((array) $config->get('selected_roles'));
    }

    if ($config->get('selection_method') === 'exclude') {
      $new_roles = array_diff($role_ids, $config->get('selected_roles'));
    }
    else {
      $new_roles = array_values((array) $config->get('selected_roles'));
    }

    foreach (array_diff($original_roles, $new_roles) as $role_id_deleted) {
      $this->deleteTestAccount($role_id_deleted);
    }

    foreach (array_diff($new_roles, $original_roles) as $role_id_added) {
      $this->createTestAccount($role_id_added);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setRoleTestAccountsPassword($password) {
    foreach ($this->getAllRoleTestAccounts() as $user) {
      $user->setPassword($password)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAllRoleTestAccounts() {
    $role_ids = $this->entityTypeManager->getStorage('user_role')->getQuery()->execute();
    if (empty($role_ids)) {
      return [];
    }

    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = [];

    array_walk($role_ids, function (&$value, $key) {
      $value = 'test.' . $value;
    });
    $query = $user_storage->getQuery();
    $user_ids = $query
      ->accessCheck(FALSE)
      ->condition('name', $role_ids, 'IN')
      ->execute();

    if (!empty($user_ids)) {
      $users = $user_storage->loadMultiple($user_ids);
    }
    return $users;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatusForRoleTestAccounts() {
    $block_after = $this->config->get('block_after');

    // Activate all test accounts if the option is set to 0 seconds.
    if ($block_after === 0) {
      foreach ($this->getAllRoleTestAccounts() as $user) {
        if ($user->isBlocked()) {
          $user->activate();
          $user->save();
        }
      }
    }
    else {
      foreach ($this->getAllRoleTestAccounts() as $user) {
        $last_access = $user->getLastAccessedTime();
        if (!$last_access) {
          $last_access = $user->getCreatedTime();
        }
        if ($last_access) {
          if ($user->isActive() && ($this->time->getRequestTime() - $block_after) > $last_access) {
            $user->block();
            $user->save();
          }
          elseif ($user->isBlocked() && ($this->time->getRequestTime() - $block_after) < $last_access) {
            $user->activate();
            $user->save();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getAllowedRoleIds(): array {
    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    return array_keys($roles);
  }

}
