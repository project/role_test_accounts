<?php

namespace Drupal\Tests\role_test_accounts\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests Role Test Accounts module install and uninstall hooks.
 *
 * @group role_test_accounts
 */
class RoleTestAccountsInstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
    $this->installConfig('user');

    $role = Role::create(['id' => 'editor', 'label' => 'Editor']);
    $role->save();

    $this->config('system.site')
      ->set('langcode', 'en')
      ->set('mail', 'site@example.com')
      ->save();
    $this->container->get('kernel')->rebuildContainer();
  }

  /**
   * Tests Role Test Accounts module install and uninstall hooks.
   */
  public function testInstallUninstallHooks() {
    $this->assertFalse(user_load_by_name('test.authenticated'));
    $this->assertFalse(user_load_by_name('test.editor'));

    $this->container->get('module_installer')->install(['role_test_accounts']);
    $this->assertNotFalse(user_load_by_name('test.authenticated'));
    $this->assertNotFalse(user_load_by_name('test.editor'));

    $this->container->get('module_installer')->uninstall(['role_test_accounts']);
    $this->assertFalse(user_load_by_name('test.authenticated'));
    $this->assertFalse(user_load_by_name('test.editor'));
  }

}
